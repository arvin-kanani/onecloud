package company.ak.ew;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import company.ak.ew.Models.Forecast;

public class DaysWeatherAdapter extends BaseAdapter {
    Context mContext;
    List<Forecast> forecasts;

    public DaysWeatherAdapter(Context mContext, List<Forecast> forecasts) {
        this.mContext = mContext;
        this.forecasts = forecasts;
    }

    @Override
    public int getCount() {
        return forecasts.size();
    }

    @Override
    public Object getItem(int i) {
        return forecasts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(mContext).inflate(R.layout.tempurature_list_item,
                viewGroup, false);

        TextView Day = view1.findViewById(R.id.Day);
        TextView Hight = view1.findViewById(R.id.Hight);
        TextView Low = view1.findViewById(R.id.Low);
        ImageView img = view1.findViewById(R.id.ImageV);
        TextView how = view1.findViewById(R.id.how);
        TextView date = view1.findViewById(R.id.date);

        Day.setText(forecasts.get(i).getDay());
        String fH = forecasts.get(i).getHigh();
        Hight.setText(PublicMethods.FtoC(fH));

        String fL = forecasts.get(i).getLow();
        Low.setText(PublicMethods.FtoC(fL));

        date.setText(forecasts.get(i).getDate());
        how.setText(forecasts.get(i).getText());

        if (forecasts.get(i).getCode().equals("0")) {
            PublicMethods.Glide(mContext, "https://cdn2.iconfinder.com/" +
                    "data/icons/freecns-cumulus/32/519909-102_Tornado-512.png", img);
        } else if (forecasts.get(i).getCode().equals("2")) {
            PublicMethods.Glide(mContext, "https://cdn2.iconfinder.com/data/" +
                    "icons/lil-weather/177/hurricane-512.png", img);
        } else if (forecasts.get(i).getCode().equals("4")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/icons/eldorado-" +
                    "stroke-weather/40/rain_lightning-512.png", img);
        } else if (forecasts.get(i).getCode().equals("3")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/icons/eldorado-" +
                    "stroke-weather/40/rain_lightning-512.png", img);
        } else if (forecasts.get(i).getCode().equals("37")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/icons/eldorado-" +
                    "stroke-weather/40/rain_lightning-512.png", img);
        } else if (forecasts.get(i).getCode().equals("38")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/icons/eldorado-" +
                    "stroke-weather/40/rain_lightning-512.png", img);
        } else if (forecasts.get(i).getCode().equals("39")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/icons/eldorado-" +
                    "stroke-weather/40/rain_lightning-512.png", img);
        } else if (forecasts.get(i).getCode().equals("47")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/icons/eldorado-" +
                    "stroke-weather/40/rain_lightning-512.png", img);
        } else if (forecasts.get(i).getCode().equals("5")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/" +
                    "icons/chubby-weather/439/rain_snow-512.png", img);
        } else if (forecasts.get(i).getCode().equals("6")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/icons/" +
                    "chubby-weather/439/rain_snow-512.png", img);
        } else if (forecasts.get(i).getCode().equals("7")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/icons/" +
                    "chubby-weather/439/rain_snow-512.png", img);
        } else if (forecasts.get(i).getCode().equals("17")) {
            PublicMethods.Glide(mContext, "https://cdn4.iconfinder.com/data/icons/" +
                    "weather-103/100/Minio_Weather_Bold-55-512.png", img);
        } else if (forecasts.get(i).getCode().equals("8")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/icons/" +
                    "chubby-weather/403/rain_less-512.png", img);
        } else if (forecasts.get(i).getCode().equals("9")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/icons/" +
                    "chubby-weather/403/rain_less-512.png", img);
        } else if (forecasts.get(i).getCode().equals("16")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/" +
                    "icons/meteocons/512/snow-512.png", img);
        } else if (forecasts.get(i).getCode().equals("19")) {
            PublicMethods.Glide(mContext, "https://cdn1.iconfinder.com/data/" +
                    "icons/weather-outline-7/50/Weather_Outline-32-512.png", img);
        } else if (forecasts.get(i).getCode().equals("20")) {
            PublicMethods.Glide(mContext, "https://cdn2.iconfinder.com/data/icons/" +
                    "weather-line-icons-vol-1/48/050-512.png", img);
        } else if (forecasts.get(i).getCode().equals("21")) {
            PublicMethods.Glide(mContext, "https://cdn2.iconfinder.com/data/icons/" +
                    "weather-line-icons-vol-1/48/050-512.png", img);
        } else if (forecasts.get(i).getCode().equals("22")) {
            PublicMethods.Glide(mContext, "https://cdn2.iconfinder.com/data/icons/" +
                    "weather-line-icons-vol-1/48/050-512.png", img);
        } else if (forecasts.get(i).getCode().equals("23")) {
            PublicMethods.Glide(mContext, "https://cdn4.iconfinder.com/data/icons/" +
                    "weather-meteorology-1/32/weather-wind-512.png", img);
        } else if (forecasts.get(i).getCode().equals("24")) {
            PublicMethods.Glide(mContext, "https://cdn4.iconfinder.com/data/icons/" +
                    "weather-meteorology-1/32/weather-wind-512.png", img);
        } else if (forecasts.get(i).getCode().equals("25")) {
            PublicMethods.Glide(mContext, "https://cdn2.iconfinder.com/data/icons/" +
                    "weather-74/24/weather-24-512.png", img);
        } else if (forecasts.get(i).getCode().equals("26")) {
            PublicMethods.Glide(mContext, "https://cdn4.iconfinder.com/data/" +
                    "icons/ionicons/512/icon-ios7-cloudy-outline-512.png", img);
        } else if (forecasts.get(i).getCode().equals("27")) {
            PublicMethods.Glide(mContext, "https://cdn4.iconfinder.com/data/" +
                    "icons/ionicons/512/icon-ios7-cloudy-outline-512.png", img);
        } else if (forecasts.get(i).getCode().equals("28")) {
            PublicMethods.Glide(mContext, "https://cdn4.iconfinder.com/data/" +
                    "icons/ionicons/512/icon-ios7-cloudy-outline-512.png", img);
        } else if (forecasts.get(i).getCode().equals("29")) {
            PublicMethods.Glide(mContext, "https://cdn4.iconfinder.com/data/" +
                    "icons/ionicons/512/icon-ios7-cloudy-outline-512.png", img);
        } else if (forecasts.get(i).getCode().equals("30")) {
            PublicMethods.Glide(mContext, "https://cdn4.iconfinder.com/data/" +
                    "icons/ionicons/512/icon-ios7-cloudy-outline-512.png", img);
        } else if (forecasts.get(i).getCode().equals("31")) {
            PublicMethods.Glide(mContext, "https://cdn4.iconfinder.com/data/" +
                    "icons/neutro-nature/32/moon-512.png", img);
        } else if (forecasts.get(i).getCode().equals("32")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/" +
                    "data/icons/weather-icons-8/512/weather-sunny-512.png", img);
        } else if (forecasts.get(i).getCode().equals("33")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/icons/" +
                    "weather-icons-8/512/weather-sunny-512.png", img);
        } else if (forecasts.get(i).getCode().equals("34")) {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/icons/" +
                    "weather-icons-8/512/weather-sunny-512.png", img);
        } else if (forecasts.get(i).getCode().equals("35")) {
            PublicMethods.Glide(mContext, "https://cdn4.iconfinder.com/data/icons/" +
                    "vectory-weather-1/40/hail_and_rain-512.png", img);
        } else if (forecasts.get(i).getCode().equals("36")) {
            PublicMethods.Glide(mContext, "https://cdn2.iconfinder.com/data/icons/" +
                    "weather-74/24/weather-26-512.png", img);
        } else if (forecasts.get(i).getCode().equals("12")) {
            PublicMethods.Glide(mContext, "https://cdn2.iconfinder.com/data/icons/" +
                    "weather-and-climate-icon/512/rain_rains-512.png", img);
        } else if (forecasts.get(i).getCode().equals("11")) {
            PublicMethods.Glide(mContext, "https://cdn2.iconfinder.com/data/icons/" +
                    "weather-and-climate-icon/512/rain_rains-512.png", img);
        } else {
            PublicMethods.Glide(mContext, "https://cdn3.iconfinder.com/data/icons/" +
                    "watchify-v1-0-32px/32/delete-512.png", img);
        }


        return view1;
    }
}

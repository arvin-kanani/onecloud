package company.ak.ew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.orhanobut.hawk.Hawk;

import org.json.JSONObject;

import java.util.List;

import company.ak.ew.Models.Forecast;
import company.ak.ew.Models.YahooWeather;
import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {
    //    ImageView ageV;
    TextView citytname, temp, how, highn, lown, daten;
    ListView ForecastListV;
    ProgressDialog Dialog;
    EditText citynameEdit;
    String IP_API_URL = "http://ip-api.com/json";
    Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Binding();
        getCityName();
        findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cityName = citynameEdit.getText() + "";
                getCityTempYahoo(cityName);
                citytname.setText(cityName);
            }
        });
        findViewById(R.id.temponmap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                startActivity(intent);
            }
        });
    }

    void Binding() {
        citytname = findViewById(R.id.cityname);
        temp = findViewById(R.id.temp);
        how = findViewById(R.id.how);
        Dialog = new ProgressDialog(mContext);
        Dialog.setTitle("Loading");
        Dialog.setMessage("Please wait to load data");
        ForecastListV = findViewById(R.id.ForecastListV);
        citynameEdit = findViewById(R.id.citynameEdit);
        highn = findViewById(R.id.hightn);
        lown = findViewById(R.id.lown);
        daten = findViewById(R.id.dates);
    }

    void getCityName() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(IP_API_URL, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseAndSetCity(responseString);
            }
        });
    }

    void parseAndSetCity(String Response) {
        try {
            Dialog.show();
            JSONObject jsonObject = new JSONObject(Response);
            String cityVal = jsonObject.getString("city").toLowerCase();
            String city = cityVal.replace("ā", "a");
            citytname.setText(city);

            getCityTempYahoo(city);

        } catch (Exception ex) {
            PublicMethods.Toast(mContext, "Error");
        }
    }

    void getCityTempYahoo(String cityName) {
        try {
            Dialog.show();
            String URL = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"
                    + cityName + "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
            AsyncHttpClient client = new AsyncHttpClient();
            client.get(URL, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    PublicMethods.Toast(mContext, "Error in connection");
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    parseSetTemp(responseString);
                    getStatus(responseString);
                    getWeatherCode(responseString);
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    Dialog.dismiss();
                }
            });


        } catch (Exception ex) {
            PublicMethods.Toast(mContext, "Error");
        }


    }

    void parseSetTemp(String Response) {
        Gson gson = new Gson();
        YahooWeather yahooWeather = gson.fromJson(Response, YahooWeather.class);
        String tempVa = yahooWeather.getQuery().getResults().getChannel().getItem().getCondition()
                .getTemp();
        String h = yahooWeather.getQuery().getResults().getChannel().getItem()
                .getForecast().get(0).getHigh();

        String l = yahooWeather.getQuery().getResults().getChannel().getItem()
                .getForecast().get(0).getLow();

        String date = yahooWeather.getQuery().getResults().getChannel().getItem()
                .getForecast().get(0).getDate();


        temp.setText(PublicMethods.FtoC(tempVa));
        highn.setText(PublicMethods.FtoC(h));
        lown.setText(PublicMethods.FtoC(l));
        daten.setText(date);


        List<Forecast> forecasts = yahooWeather.getQuery().getResults()
                .getChannel().getItem().getForecast();
        DaysWeatherAdapter weatherAdapter = new DaysWeatherAdapter(mContext, forecasts);
        ForecastListV.setAdapter(weatherAdapter);
    }


    void getStatus(String Response) {
        Gson gson = new Gson();
        YahooWeather yahooWeather = gson.fromJson(Response, YahooWeather.class);
        String tempVa = yahooWeather.getQuery().getResults().getChannel().getItem().getCondition().getText();
        how.setText(tempVa);

    }

    void getWeatherCode(String Response) {
        Gson gson = new Gson();
        YahooWeather yahooWeather = gson.fromJson(Response, YahooWeather.class);
        String tempVa = yahooWeather.getQuery().getResults().getChannel().getItem()
                .getCondition().getCode();

//        if (tempVa.equals("0")) {
//            PublicMethods.Glide(mContext, "https://wallportal.com/uploads/posts/" +
//                    "live-tornado-wallpaper/live_tornado_wallpaper_006.jpg", ageV);
//        }
//        if (tempVa.equals("2")) {
//            PublicMethods.Glide(mContext, "http://www.freeapplewallpapers.com/wp-content/" +
//                    "uploads/2013/06/Large-Cloud-Hurricane.jpg", ageV);
//
//        }
//        if (tempVa.equals("16")) {
//            PublicMethods.Glide(mContext, "https://static-s.aa-cdn.net/img/gp/" +
//                    "20600003762870/JBP0wcPWQyZqdNzGnNi4w5q0Us2VTvkDcYlbYTKtitvCtyaMk3jgYu92" +
//                    "pk_UCku6P3XM=h900", ageV);
//        }
//        if (tempVa.equals("20")) {
//            PublicMethods.Glide(mContext, "https://images.unsplash.com/photo-" +
//                    "1507513319174-e556268bb244?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=" +
//                    "8547d532aae09d0c9413ad8e1c7b6627&w=1000&q=80", ageV);
//        }
//        if (tempVa.equals("17")) {
//            PublicMethods.Glide(mContext, "https://i.pinimg.com/originals/d2/ee/15/" +
//                    "d2ee155ddd85af690d06bb8cf06e7c88.jpg", ageV);
//
//        }
//        if (tempVa.equals("26")) {
//            PublicMethods.Glide(mContext, "https://i.pinimg.com/originals/c5/4f/" +
//                    "5c/c54f5c95272b9f2edfdcc2ebd6d5a7b4.jpg", ageV);
//        }
//        if (tempVa.equals("27")) {
//            PublicMethods.Glide(mContext, "https://i.pinimg.com/originals/c5/4f/5c/" +
//                    "c54f5c95272b9f2edfdcc2ebd6d5a7b4.jpg", ageV);
//        }
//        if (tempVa.equals("28")) {
//            PublicMethods.Glide(mContext, "https://i.pinimg.com/originals/c5/4f/5c/" +
//                    "c54f5c95272b9f2edfdcc2ebd6d5a7b4.jpg", ageV);
//        }
//        if (tempVa.equals("29")) {
//            PublicMethods.Glide(mContext, "https://i.pinimg.com/originals/c5/4f/5c/" +
//                    "c54f5c95272b9f2edfdcc2ebd6d5a7b4.jpg", ageV);
//        }
//        if (tempVa.equals("30")) {
//            PublicMethods.Glide(mContext, "https://i.pinimg.com/originals/c5/4f/5c/" +
//                    "c54f5c95272b9f2edfdcc2ebd6d5a7b4.jpg", ageV);
//        }
//        if (tempVa.equals("32")) {
//            PublicMethods.Glide(mContext, "http://apple.wallpapersfine.com/wallpapers" +
//                    "/original/750x1334/w-12367.jpg", ageV);
//        }
//        if (tempVa.equals("5")) {
//            PublicMethods.Glide(mContext, "https://i.pinimg.com/originals/d7/" +
//                    "]2e/d5/d72ed5be5345c62f161e34f89d08552d.jpg", ageV);
//        }
//        if (tempVa.equals("10")) {
//            PublicMethods.Glide(mContext, "https://i.pinimg.com/originals/d7/2e/" +
//                    "d5/d72ed5be5345c62f161e34f89d08552d.jpg", ageV);
//        }
//        if (tempVa.equals("3")) {
//            PublicMethods.Glide(mContext, "https://i.pinimg.com/originals/4a/70/" +
//                    "48/4a7048ef4dbafa2d3c6d31b4484156d4.jpg", ageV);
//        }
//        if (tempVa.equals("4")) {
//            PublicMethods.Glide(mContext, "https://i.pinimg.com/originals/4a/70/" +
//                    "48/4a7048ef4dbafa2d3c6d31b4484156d4.jpg", ageV);
//        }
//        if (tempVa.equals("37")) {
//            PublicMethods.Glide(mContext, "https://i.pinimg.com/originals/4a/70/" +
//                    "48/4a7048ef4dbafa2d3c6d31b4484156d4.jpg", ageV);
//        }
//        if (tempVa.equals("38")) {
//            PublicMethods.Glide(mContext, "https://i.pinimg.com/originals/4a/70/" +
//                    "48/4a7048ef4dbafa2d3c6d31b4484156d4.jpg", ageV);
//        }
//        if (tempVa.equals("47")) {
//            PublicMethods.Glide(mContext, "https://i.pinimg.com/originals/4a/70/" +
//                    "48/4a7048ef4dbafa2d3c6d31b4484156d4.jpg", ageV);
//        }
    }

}


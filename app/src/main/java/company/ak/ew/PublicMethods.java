package company.ak.ew;

import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.text.DecimalFormat;

public class PublicMethods {
    static void Toast(Context mContext, String Text){
        Toast.makeText(mContext, Text, Toast.LENGTH_SHORT).show();
    }
    static String FtoC(String F){
        String result = "0";
        int temp = Integer.parseInt(F);
        int cTemp = (int) ((temp - 32)/1.8);
        result  = Integer.toString(cTemp) + "°";
        return result;
    }

    static void Glide(Context mContext, String URL, ImageView img){
        Glide.with(mContext).load(URL).into(img);
    }
}
